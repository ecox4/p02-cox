//
//  ViewController.h
//  p02-cox
//
//  Created by Em on 2/1/17.
//  Copyright © 2017 Em. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface ViewController : UIViewController{
    UILabel *t0,*t1,*t2,*t3,*t4,*t5,*t6,*t7,*t8,*t9,*t10,*t11,*t12,*t13,*t14,*t15;

}

@property (nonatomic,strong) IBOutlet UILabel *t0,*t1,*t2,*t3,*t4,*t5,*t6,*t7,*t8,*t9,*t10,*t11,*t12,*t13,*t14,*t15;

@property (nonatomic,strong) IBOutlet UILabel *score;

-(IBAction)reset;
-(IBAction)upOp;
-(IBAction)leftOp;
-(IBAction)rightOp;
-(IBAction)downOp;

@end

