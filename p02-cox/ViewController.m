//
//  ViewController.m
//  p02-cox
//
//  Created by Em on 2/1/17.
//  Copyright © 2017 Em. All rights reserved.
//

#include<time.h>
#include <stdlib.h>
#import "ViewController.h"

@interface ViewController ()


@end

@implementation ViewController
@synthesize t0,t1,t2,t3,t4,t5,t6,t7,t8,t9,t10,t11,t12,t13,t14,t15;
@synthesize score;

int tileData[4][4] = {{0,0,0,0},{0,0,0,0},{0,0,0,0},{0,0,0,0}};
int scoreVal = 0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    t0 = (UILabel *)[self.view viewWithTag:1];
    t1 = (UILabel *)[self.view viewWithTag:2];
    t2 = (UILabel *)[self.view viewWithTag:3];
    t3 = (UILabel *)[self.view viewWithTag:4];
    t4 = (UILabel *)[self.view viewWithTag:5];
    t5 = (UILabel *)[self.view viewWithTag:6];
    t6 = (UILabel *)[self.view viewWithTag:7];
    t7 = (UILabel *)[self.view viewWithTag:8];
    t8 = (UILabel *)[self.view viewWithTag:9];
    t9 = (UILabel *)[self.view viewWithTag:10];
    t10 = (UILabel *)[self.view viewWithTag:11];
    t11 = (UILabel *)[self.view viewWithTag:12];
    t12 = (UILabel *)[self.view viewWithTag:13];
    t13 = (UILabel *)[self.view viewWithTag:14];
    t14 = (UILabel *)[self.view viewWithTag:15];
    t15 = (UILabel *)[self.view viewWithTag:16];
    
    score = (UILabel *)[self.view viewWithTag:17];
    [self reset];
}
-(void)updateTiles{
    t0.text = [NSString stringWithFormat:@"%d",tileData[0][0]];
    t1.text = [NSString stringWithFormat:@"%d",tileData[0][1]];
    t2.text = [NSString stringWithFormat:@"%d",tileData[0][2]];
    t3.text = [NSString stringWithFormat:@"%d",tileData[0][3]];
    
    t4.text = [NSString stringWithFormat:@"%d",tileData[1][0]];
    t5.text = [NSString stringWithFormat:@"%d",tileData[1][1]];
    t6.text = [NSString stringWithFormat:@"%d",tileData[1][2]];
    t7.text = [NSString stringWithFormat:@"%d",tileData[1][3]];
    
    t8.text = [NSString stringWithFormat:@"%d",tileData[2][0]];
    t9.text = [NSString stringWithFormat:@"%d",tileData[2][1]];
    t10.text = [NSString stringWithFormat:@"%d",tileData[2][2]];
    t11.text = [NSString stringWithFormat:@"%d",tileData[2][3]];

    t12.text = [NSString stringWithFormat:@"%d",tileData[3][0]];
    t13.text = [NSString stringWithFormat:@"%d",tileData[3][1]];
    t14.text = [NSString stringWithFormat:@"%d",tileData[3][2]];
    t15.text = [NSString stringWithFormat:@"%d",tileData[3][3]];

    score.text =[NSString stringWithFormat:@"%d",scoreVal];

}

-(void)insertTile{
    bool success = false;
    while(!success){
        srand(time(NULL));
        int x = rand()%4;
        int y = rand()%4;
        if(tileData[x][y] == 0){
            if(x > 1){
                tileData[x][y] = 4;
                success = true;
            }else{
                tileData[y][x] = 2;
                success = true;
            }
        }
    }
}

-(IBAction)reset{
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            tileData[i][j] = 0;
        }
    }
    for(int i = 0; i < 3; ++i){
        [self insertTile];
    }
    scoreVal = 0;
    [self updateTiles];
}

//code for shifting repurposed/modified from chandruscm.wordpress.com/2014/10/25/2048-in-c-c/
-(void)shiftD{
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            if(tileData[j][i] == 0){
                for(int k = 0; k < 4; ++k){
                    if(tileData[k][i] > 0){
                        tileData[j][i] = tileData[k][i];
                        tileData[k][i] = 0;
                        break;
                    }
                }
            }
        }
    }
}
-(void)shiftU{
    for(int i = 0; i < 4; ++i){
        for(int j = 3; j >= 0; --j){
            if(tileData[j][i] == 0){
                for(int k = 3; k >= 0; --k){
                    if(tileData[k][i] > 0){
                        tileData[j][i] = tileData[k][i];
                        tileData[k][i] = 0;
                        break;
                    }
                }
            }
        }
    }
}
-(void)shiftR{
    for(int i = 0; i < 4; ++i){
        for(int j = 0; j < 4; ++j){
            if(tileData[i][j] == 0){
                for(int k = 0; k < 4; ++k){
                    if(tileData[i][k] > 0){
                        tileData[i][j] = tileData[i][k];
                        tileData[i][k] = 0;
                        break;
                    }
                }
            }
        }
    }
}
-(void)shiftL{
    for(int i = 0; i < 4; ++i){
        for(int j = 3; j >= 0; --j){
            if(tileData[i][j] == 0){
                for(int k = 3; k >= 0; --k){
                    if(tileData[i][k] > 0){
                        tileData[i][j] = tileData[i][k];
                        tileData[i][k] = 0;
                        break;
                    }
                }
            }
        }
    }
}
-(IBAction)upOp{
    [self shiftU];
    for(int i = 0; i < 4; ++i){
    //scan L -> R
        if(tileData[1][i] == tileData[0][i]){
            tileData[0][i] *= 2;
            tileData[1][i] = 0;
            if(tileData[3][i] == tileData[2][i]){
                tileData[1][i] = tileData[3][i] + tileData[2][i];
                tileData[2][i] = 0;
                tileData[3][i] = 0;
                scoreVal++;
            }
            scoreVal++;
        }else if(tileData[1][i] == tileData[2][i]){
            tileData[1][i] *= 2;
            tileData[2][i] = 0;
            scoreVal++;
        }else if(tileData[2][i] == tileData[3][i]){
            tileData[2][i] *= 2;
            tileData[3][i] = 0;
            scoreVal++;
        }
    }
    [self shiftU];
    [self insertTile];
    [self updateTiles];
}
-(IBAction)downOp{
    [self shiftD];
    for(int i = 0; i < 4; ++i){
    //scan L -> R
        if(tileData[3][i] == tileData[2][i]){
            tileData[3][i] *= 2;
            tileData[2][i] = 0;
            if(tileData[1][i] == tileData[0][i]){
                tileData[2][i] = tileData[1][i] * tileData[0][i];
                tileData[1][i] = 0;
                tileData[0][i] = 0;
                scoreVal++;
            }
            scoreVal++;
        }else if(tileData[1][i] == tileData[2][i]){
            tileData[2][i] *= 2;
            tileData[1][i] = 0;
            scoreVal++;
        }else if(tileData[0][i] == tileData[1][i]){
            tileData[1][i] *= 2;
            tileData[0][i] = 0;
            scoreVal++;
        }
    }
    [self shiftD];
    [self insertTile];
    [self updateTiles];
}
-(IBAction)leftOp{
    [self shiftL];
    for(int i = 0; i < 4; ++i){
    //scan top -> bottom
        if(tileData[i][1] == tileData[i][0]){
            tileData[i][0] *= 2;
            tileData[i][1] = 0;
            if(tileData[i][3] == tileData[i][2]){
                tileData[i][1] = tileData[i][3] * tileData[i][2];
                tileData[i][2] = 0;
                tileData[i][3] = 0;
                scoreVal++;
            }
            scoreVal++;
        }else if(tileData[1][i] == tileData[2][i]){
            tileData[i][1] *= 2;
            tileData[i][2] = 0;
            scoreVal++;
        }else if(tileData[i][2] == tileData[i][3]){
            tileData[i][2] *= 2;
            tileData[i][3] = 0;
            scoreVal++;
        }
    }
    [self shiftL];
    [self insertTile];
    [self updateTiles];
}
-(IBAction)rightOp{
    [self shiftR];
    for(int i = 0; i < 4; ++i){
    //scan top -> bottom
        if(tileData[i][3] == tileData[i][2]){
            tileData[i][3] *= 2;
            tileData[i][2] = 0;
            if(tileData[i][1] == tileData[i][0]){
                tileData[i][2] = tileData[i][1] * tileData[i][0];
                tileData[i][1] = 0;
                tileData[i][0] = 0;
                scoreVal++;
            }
            scoreVal++;
        }else if(tileData[i][1] == tileData[i][2]){
            tileData[i][2] *= 2;
            tileData[i][1] = 0;
            scoreVal++;
        }else if(tileData[i][0] == tileData[i][1]){
            tileData[i][1] *= 2;
            tileData[i][0] = 0;
            scoreVal++;
        }
    }
    [self shiftR];
    [self insertTile];
    [self updateTiles];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


@end
